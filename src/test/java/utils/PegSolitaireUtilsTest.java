/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test.java.utils;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.Point;
import java.util.Arrays;
import java.util.BitSet;

import org.junit.jupiter.api.Test;

import main.java.utils.Move;
import main.java.utils.PegSolitaireUtils;
import main.java.utils.PrintPegSolitairBoard;

/**
 *
 * PegSolitaireUtilsTest.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 27 Oct 2021
 *
 */
class PegSolitaireUtilsTest {

	private static String[] boards = {
			"111111111111111101111111111111111",  //empty center
			"000000000000000000000000000000000",  //all empty
			"111111111111111111111111111111111",  //all pegs
			"000000000111111101111111111111111"  //leading 0s
	};

	private final static char  EMPTY = PrintPegSolitairBoard.EMPTY,
			PEG = PrintPegSolitairBoard.PEG, BORDER = PrintPegSolitairBoard.BORDER;

	private final char[][] emptyCenter ={
			{BORDER, BORDER,PEG,PEG,PEG,BORDER,BORDER},
			{BORDER, BORDER,PEG,PEG,PEG,BORDER,BORDER},
			{PEG,PEG,PEG,PEG,PEG,PEG,PEG},
			{PEG,PEG,PEG,EMPTY,PEG,PEG,PEG},
			{PEG,PEG,PEG,PEG,PEG,PEG,PEG},
			{BORDER, BORDER,PEG,PEG,PEG,BORDER,BORDER},
			{BORDER, BORDER,PEG,PEG,PEG,BORDER,BORDER},
	};

	private final char[][] allEmpty ={
			{BORDER, BORDER,EMPTY,EMPTY,EMPTY,BORDER,BORDER},
			{BORDER, BORDER,EMPTY,EMPTY,EMPTY,BORDER,BORDER},
			{EMPTY,EMPTY,EMPTY,EMPTY,EMPTY,EMPTY,EMPTY},
			{EMPTY,EMPTY,EMPTY,EMPTY,EMPTY,EMPTY,EMPTY},
			{EMPTY,EMPTY,EMPTY,EMPTY,EMPTY,EMPTY,EMPTY},
			{BORDER, BORDER,EMPTY,EMPTY,EMPTY,BORDER,BORDER},
			{BORDER, BORDER,EMPTY,EMPTY,EMPTY,BORDER,BORDER},
	};

	private final char[][] allPegs ={
			{BORDER, BORDER,PEG,PEG,PEG,BORDER,BORDER},
			{BORDER, BORDER,PEG,PEG,PEG,BORDER,BORDER},
			{PEG,PEG,PEG,PEG,PEG,PEG,PEG},
			{PEG,PEG,PEG,PEG,PEG,PEG,PEG},
			{PEG,PEG,PEG,PEG,PEG,PEG,PEG},
			{BORDER, BORDER,PEG,PEG,PEG,BORDER,BORDER},
			{BORDER, BORDER,PEG,PEG,PEG,BORDER,BORDER},
	};

	private final char[][] leading0s ={
			{BORDER, BORDER,EMPTY,EMPTY,EMPTY,BORDER,BORDER},
			{BORDER, BORDER,EMPTY,EMPTY,EMPTY,BORDER,BORDER},
			{EMPTY,EMPTY,EMPTY,PEG,PEG,PEG,PEG},
			{PEG,PEG,PEG,EMPTY,PEG,PEG,PEG},
			{PEG,PEG,PEG,PEG,PEG,PEG,PEG},
			{BORDER, BORDER,PEG,PEG,PEG,BORDER,BORDER},
			{BORDER, BORDER,PEG,PEG,PEG,BORDER,BORDER},
	};

	private static int[] indecies = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32};

	/**
	 * Test method for {@link main.java.utils.PegSolitaireUtils#PegSolitaireUtils(java.lang.String)}.
	 */
	@Test
	void testPegSolitaireUtilsUtiles() {

	}

	/**
	 * Test method for {@link main.java.utils.PegSolitaireUtils#fromString(java.lang.String)}.
	 */
	@Test
	void testFromString() {

		String expected ="{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32}";
		assertEquals(PegSolitaireUtils.boardFromString(boards[0]).toString(), expected);

		try {
			PegSolitaireUtils.boardFromString(null);//should throw IllegalArgumentException
			assertTrue("null argument should throw IllegalArgumentException",false);
		} catch (IllegalArgumentException ex) {}

		try {
			PegSolitaireUtils.boardFromString("");//should throw IllegalArgumentException
			assertTrue("wrong length argument should throw IllegalArgumentException",false);
		} catch (IllegalArgumentException ex) {}

		try {
			PegSolitaireUtils.boardFromString("111111111111");//should throw IllegalArgumentException
			assertTrue("wrong length argument 111111111111 should throw IllegalArgumentException",false);
		} catch (IllegalArgumentException ex) {}

		try {
			PegSolitaireUtils.boardFromString("111111111111");//should throw IllegalArgumentException
			assertTrue("wrong length argument should throw IllegalArgumentException",false);
		} catch (IllegalArgumentException ex) {}

		try {
			PegSolitaireUtils.boardFromString("111111111111");//should throw IllegalArgumentException
			assertTrue("wrong number of peg holes should throw IllegalArgumentException",false);
		} catch (IllegalArgumentException ex) {}
	}

	/**
	 * Test method for {@link main.java.utils.PegSolitaireUtils#boardCharRepresentation(java.util.BitSet)}.
	 */
	@Test
	void testboardCharRepresentation(){

		BitSet eCenter = PegSolitaireUtils.boardFromString(boards[0]);
		char[][] charRep = PrintPegSolitairBoard.boardCharRepresentation(eCenter);
		assertTrue(Arrays.deepEquals(charRep, emptyCenter));

		BitSet empty = PegSolitaireUtils.boardFromString(boards[1]);
		charRep = PrintPegSolitairBoard.boardCharRepresentation(empty);
		assertTrue(Arrays.deepEquals(charRep, allEmpty));

		BitSet full = PegSolitaireUtils.boardFromString(boards[2]);
		charRep = PrintPegSolitairBoard.boardCharRepresentation(full);
		assertTrue(Arrays.deepEquals(charRep, allPegs));

		BitSet lead0s = PegSolitaireUtils.boardFromString(boards[3]);
		charRep = PrintPegSolitairBoard.boardCharRepresentation(lead0s);
		assertTrue(Arrays.deepEquals(charRep, leading0s));
	}

	/**
	 * Test method for {@link main.java.utils.PegSolitaireUtils#indexToXY(BitSet, int)}.
	 */
	@Test
	void testIndexToXY(){

		for(int index : indecies){
			Point p1 = PegSolitaireUtils.indexToXY(indecies[index]);
			Point expected = indexToXY(indecies[index]);
			assertTrue("expected: "+expected+" actual: "+ p1 +" for index = "+index, p1.y==expected.y && p1.x==expected.x);
		}

		try {
			 PegSolitaireUtils.indexToXY(-1);//should throw IllegalArgumentException
			assertTrue("Invalid index should throw IllegalArgumentException",false);
		} catch (IllegalArgumentException ex) {}

		try {
			 PegSolitaireUtils.indexToXY( PegSolitaireUtils.NUMBER_OF_PEG_HOLES);//should throw IllegalArgumentException
			assertTrue("Invalid index should throw IllegalArgumentException",false);
		} catch (IllegalArgumentException ex) {}
	}

	/**
	 * Test method for {@link main.java.utils.PegSolitaireUtils#xyToIndex(int, int)}.
	 */
	@Test
	void testXYToIndex(){

		for(int x =0; x < PegSolitaireUtils.BOARD_SIZE; x++){
			for(int y =0; y < PegSolitaireUtils.BOARD_SIZE; y++){
				if(!PegSolitaireUtils.isWithinBoard(x, y)) {
					continue;
				}
				int index = PegSolitaireUtils.xyToIndex(x, y);
				int expected = xyToIndex(x, y);
				assertTrue("expected: "+expected+" actual: "+ index +" for x="+x+" y="+y, index  == expected);
			}
		}

		try {
			PegSolitaireUtils.xyToIndex(0,0);//should throw IllegalArgumentException
			assertTrue("Not witin board.should throw IllegalArgumentException",false);
		} catch (IllegalArgumentException ex) {}
	}

	/**
	 * Test method for {@link main.java.utils.PegSolitaireUtils#canMove(BitSet, int, main.java.utils.Move)}.
	 */
	@Test
	void testCanMove(){

		String s = boards[0];  //empty center
		BitSet board = PegSolitaireUtils.boardFromString(s);

		for(int index = 0; index < s.length(); index++){
			for(Move direction : Move.values()){
				if(index == 4 && direction == Move.DOWN
					 || index == 14 && direction == Move.RIGHT
					 || index == 18 && direction == Move.LEFT
					 || index == 28 && direction == Move.UP
					 ){
					assertTrue(PegSolitaireUtils.canMove(board, index, direction));
				}else{
					assertTrue(!PegSolitaireUtils.canMove(board, index, direction));
				}
			}
		}
	}

	/**
	 * Test method for {@link main.java.utils.PegSolitaireUtils#move(BitSet, int, Move)}.
	 */
	@Test
	void testMove(){

		String s = boards[0];  //empty center
		BitSet board = PegSolitaireUtils.boardFromString(s);
		BitSet expected = PegSolitaireUtils.boardFromString("111101111011111111111111111111111");

		PegSolitaireUtils.move(board, 4, Move.DOWN);
		assertEquals(board, expected);

		board = PegSolitaireUtils.boardFromString(s);
		expected = PegSolitaireUtils.boardFromString("111111111111110011111111111111111");
		PegSolitaireUtils.move(board, 14, Move.RIGHT);
		assertEquals(board, expected);

		board = PegSolitaireUtils.boardFromString(s);
		expected = PegSolitaireUtils.boardFromString("111111111111111110011111111111111");
		PegSolitaireUtils.move(board, 18, Move.LEFT);
		assertEquals(board, expected);

		board = PegSolitaireUtils.boardFromString(s);
		expected = PegSolitaireUtils.boardFromString("111111111111111111111110111101111");
		PegSolitaireUtils.move(board, 28, Move.UP);
		assertEquals(board, expected);

		expected = PegSolitaireUtils.boardFromString("111111111111111111111111111101101");
		PegSolitaireUtils.move(board, 31, Move.UP);
		assertEquals(board, expected);

		//performs an invalid move without throwing an error
		expected = PegSolitaireUtils.boardFromString("111111111111111111111111111100100");
		PegSolitaireUtils.move(board, 32, Move.UP);
		assertEquals(board, expected);

		try {
			PegSolitaireUtils.move(board, 32, Move.DOWN);//should throw IndexOutOfBoundsException
			assertTrue("Move not witin board.should throw IndexOutOfBoundsException",false);
		} catch (IndexOutOfBoundsException ex) {}
	}

	/**
	 * Test method for {@link main.java.utils.PegSolitaireUtils#moveIfValid(BitSet, int, Move)}.
	 */
	@Test
	void testMoveIfValid(){

		String s = boards[0];  //empty center
		BitSet board = PegSolitaireUtils.boardFromString(s);
		BitSet expected = PegSolitaireUtils.boardFromString("111101111011111111111111111111111");

		assertTrue(PegSolitaireUtils.moveIfValid(board, 4, Move.DOWN));
		assertEquals(board, expected);

		board = PegSolitaireUtils.boardFromString(s);
		expected = PegSolitaireUtils.boardFromString("111111111111110011111111111111111");
		assertTrue(PegSolitaireUtils.moveIfValid(board, 14, Move.RIGHT));
		assertEquals(board, expected);

		board = PegSolitaireUtils.boardFromString(s);
		expected = PegSolitaireUtils.boardFromString("111111111111111110011111111111111");
		assertTrue(PegSolitaireUtils.moveIfValid(board, 18, Move.LEFT));
		assertEquals(board, expected);

		board = PegSolitaireUtils.boardFromString(s);
		expected = PegSolitaireUtils.boardFromString("111111111111111111111110111101111");
		assertTrue(PegSolitaireUtils.moveIfValid(board, 28, Move.UP));
		assertEquals(board, expected);

		//invalid moves
		//System.out.println(PegSolitaireUtils.toString(board));
		expected = (BitSet) board.clone(); //copy board
		assertTrue(! PegSolitaireUtils.moveIfValid(board, 31, Move.UP));

		for(int index = 0; index < PegSolitaireUtils.NUMBER_OF_PEG_HOLES; index++){
			if(index == 9 || index == 21 || index == 25 || index == 31) {
				continue;
			}
			for (Move direction : Move.values()){
				assertTrue(! PegSolitaireUtils.moveIfValid(board, index, direction));
			}
		}
		//make sure board has not changed
		assertEquals(board, expected);
	}

	/**
	 * Test method for {@link main.java.utils.PegSolitaireUtils#numberOfDifferentBits(BitSet, BitSet)}.
	 */
	@Test
	void testNumberOfDifferentBits(){

		BitSet a = PegSolitaireUtils.boardFromString("111101111011111111111111111111111");
		assertTrue(PegSolitaireUtils.numberOfDifferentBits(a, a) == 0);

		assertTrue(PegSolitaireUtils.numberOfDifferentBits(new BitSet(), new BitSet()) ==0);

		BitSet b = (BitSet) a.clone();
		for (int i=0; i < a.length(); i++){
			assertTrue(PegSolitaireUtils.numberOfDifferentBits(b, a) == i);
			b.flip(i);
		}

		b = (BitSet) a.clone();
		for (int i=0; i < a.length(); i++){
			assertTrue(PegSolitaireUtils.numberOfDifferentBits(b, a) == i);
			a.flip(i);
		}

		try {
			PegSolitaireUtils.numberOfDifferentBits(null, a);//should throw NullPointerException
			assertTrue("null argument should throw NullPointerException",false);
		} catch (NullPointerException ex) {}
	}

	@Test
	void printEndMessage(){
		System.out.println(this.getClass().getSimpleName() +" : all done ");
	}

	//-->help methods

	/**
	 * return {@link Point} (Point(x,y)) of representing column - row in a
	 * {@link PegSolitaireUtils#NUMBER_OF_PEG_HOLES} peg-holes board
	 */
	public static Point indexToXY(int index){

		if(index < 0 || index >= PegSolitaireUtils.NUMBER_OF_PEG_HOLES)
						throw new IllegalArgumentException("Invalid index "+index);

		if(index < 3)
			return new Point(index+PegSolitaireUtils.CORNER_SIZE, 0);
		else if (index < 6)
			return new Point(index-3+PegSolitaireUtils.CORNER_SIZE, 1);
		else if (index < 13)
			return new Point(index-6, 2);
		else if (index < 20)
			return new Point(index-13, 3);
		else if (index < 27)
			return new Point(index-20, 4);
		else if (index < 30)
			return new Point(index-27+PegSolitaireUtils.CORNER_SIZE, 5);
		//else
		return new Point(index-30+PegSolitaireUtils.CORNER_SIZE, 6);
	}

	/**
	 * @return
	 * index in a {@link BitSet} representing a {@link PegSolitaireUtils#NUMBER_OF_PEG_HOLES}
	 * peg-holes board, for a given x - y (column - row) in that board.
	 */
	public static int xyToIndex(int x, int y){

		if(!PegSolitaireUtils.isWithinBoard(x, y))
			throw new IllegalArgumentException(x+"-"+y+ " is not a valid peg hole");

		if(y==0)
			return 	 x - PegSolitaireUtils.CORNER_SIZE;
		else if (y==1)
			return x + 3 - PegSolitaireUtils.CORNER_SIZE;
		else if (y==2)
			return x + 6;
		else if (y==3)
			return x + 13;
		else if (y==4)
			return x + 20;
		else if (y==5)
			return x + 27 - PegSolitaireUtils.CORNER_SIZE;
		else if (y==6)
			return x + 30 - PegSolitaireUtils.CORNER_SIZE;

		throw new IllegalStateException(); //should never be invoked
	}
}

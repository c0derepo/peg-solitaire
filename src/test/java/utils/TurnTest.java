package test.java.utils;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import main.java.utils.Move;

class TurnTest {

	@Test
	void test() {
		int x = 7;
		int y=5;

		int x1 = x, y1=y;
		int x2 = x, y2 =y;
		//apply all turn and check back to start point
		for(Move direction : Move.values()){
			x1+=direction.dx1;
			y1+=direction.dy2;

			x2+=direction.dx1;
			y2+=direction.dy2;
		}
		assertTrue(x1==x);
		assertTrue(y1==y);
		assertTrue(x2==x);
		assertTrue(y2==y);

		System.out.println(this.getClass().getSimpleName() +" : all done ");
	}

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test.java.algos;

import static org.junit.Assert.assertTrue;

import java.util.BitSet;
import java.util.List;

import main.java.algos.Algo;
import main.java.utils.PrintPegSolitairBoard;

/**
 *
 * AlgoTestUtils.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 7 Nov 2021
 *
 */
public class AlgoTestUtils {

	static void testCase(Algo algo, int testIndex, int numberOfMoves, boolean printOut){

		long startTime = 0;
		if(printOut){
			System.err.println("start board -> target board:\n");
			System.err.println(PrintPegSolitairBoard.toString(algo.getStartBoard()));
			System.err.println(PrintPegSolitairBoard.toString(algo.getTargetBoard()));
			startTime = System.currentTimeMillis();
		}

		boolean solved = algo.solve();
		assertTrue(solved);

		List<BitSet> path = algo.getSolution();
		int pathSize = path.size()-1;
		assertTrue("i="+testIndex+" wrong path length "+pathSize+". Expected "+numberOfMoves,  pathSize  == numberOfMoves);

		if(printOut){
			printDuration((double)System.currentTimeMillis()  - startTime);
			System.out.println("solved after "+ ( path.size()-1 ) +" moves\n");
			for(BitSet b: path){
				System.out.println(PrintPegSolitairBoard.toString(b) +"\n");
			}

			try {
				Thread.sleep(400);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}
	}

	static void printDuration(double duration){
		double second = 1000;//in millis
		double minute = 60*1000;//in millis
		String units = "mills";
		if(duration > second && duration <= minute ){
			duration /= second;
			units = "seconds";
		}else if(duration >  minute ){
			duration /= minute;
			units = "minutes";
		}
		System.out.println("----------------->Duration "+units+":   "+ String.format("%,.2f", duration)  );
	}
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test.java.algos;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import main.java.algos.AStar;
import main.java.algos.Algo;
import test.java.algos.test_cases.TestCase;
import test.java.algos.test_cases.TestCase1;
import test.java.algos.test_cases.TestCase2;

/**
 *
 * AStarTest.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 30 Oct 2021
 *
 */
class AStarTest {

	private static boolean printOut, runLongTest;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		printOut  = false;
		runLongTest =  true;
	}

	@Test
	/**
	 * Test method for {@link Astar#solve()}}.
	 */
	void testCase1() throws InterruptedException {

		long startTime = System.currentTimeMillis();

		TestCase test = new TestCase1();
		String start = test.startBaord();
		for(int i= 0; i<  test.targetBoards().length; i++ ){// 9
			if(printOut){
				System.out.println("\n------------>TestCase1 # "+i +" ("+test.numberOfMoves()[i]+" moves)");
			}
			Algo algo = new AStar(start, test.targetBoards()[i]);
			AlgoTestUtils.testCase(algo, i, test.numberOfMoves()[i],printOut);
		}

		double duration = (System.currentTimeMillis() - startTime)/1000;
		System.out.println(this.getClass().getSimpleName() +" testCase1 : all done ("+ duration +" seconds)");
	}

	@Test
	/**
	 * Test method for {@link Astar#solve()}}.
	 */
	void testCase2() {

		long startTime = System.currentTimeMillis();
		List<Integer> longTests = Arrays.asList(14,15);
		TestCase test = new TestCase2();
		String start = test.startBaord();

		for(int i= 0; i < test.targetBoards().length ; i++ ){

			if(longTests.contains(i) ){
				if(runLongTest){
					if(printOut) {
						System.out.println("\n------------>Long TestCase2 # "+i+ "/"+ (test.targetBoards().length -1)
								+" ("+test.numberOfMoves()[i]+" moves)");
					}
				}else{
					if(printOut) {
						System.out.println("\nSkipping long test TestCase2 # "+i +" ("+test.numberOfMoves()[i]+" moves)");
					}
					continue;
				}
			}else if(printOut){
				System.out.println("\n------------>TestCase2 # "+i +" ("+test.numberOfMoves()[i]+" moves)");
			}
			Algo algo = new AStar(start, test.targetBoards()[i]);
			AlgoTestUtils.testCase(algo,i, test.numberOfMoves()[i],printOut);
		}

		double duration = (System.currentTimeMillis() - startTime)/1000;
		System.out.println(this.getClass().getSimpleName() +" testCase2 : all done ("+ duration +" seconds)");
	}
}

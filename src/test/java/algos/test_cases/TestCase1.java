/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test.java.algos.test_cases;

/**
 *
 * TestCase1.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 31 Oct 2021
 *
 */
public class TestCase1 implements TestCase {

	static String[] targetBoards ={

			//by 1rder 1f number 1f turns
			"111011110111110111111111111111111",  //1 down
			"111011001111110111111111111111111",  //2 right
			"111011101111100111110111111111111",  //3 up
			"111111100111100011110111111111111",  //4 up
			"011011101111100011110111111111111",  //5 down
			"011011101111100011111001111111111",  //6 left
			"011011101111100011111011111011011",  //7 up
			"011011110011100011111011111011011",  //8 left
			"011011001011100011111011111011011",  //9 right BFS 1 sec DFS 1 sec
			"001001010011100011110011111001001",  //14 moves BFS 1.6 min  DFS 3 min
			"000000000100000000000000001000000",  //29 moves BFS no DFS 20 min
			"000000000000000000010000000000000"   //30 moves BFS no DFS 20 min
	};

	static String startBoard = "111111111111110011111111111111111";

	static int[] numberOfMoves = {1,2,3,4,5,6,7,8,9,14,29,30};

	@Override
	public String[] targetBoards() {
		return targetBoards;
	}

	@Override
	public String startBaord() {
		return startBoard;
	}

	@Override
	public int[] numberOfMoves() {
		return numberOfMoves;
	}
}

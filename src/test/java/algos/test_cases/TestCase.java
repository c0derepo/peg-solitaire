/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test.java.algos.test_cases;

/**
 *
 * TestCase.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 31 Oct 2021
 *
 */
public interface TestCase {

	String[] targetBoards();
	String startBaord();
	int[] numberOfMoves();
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test.java.algos.test_cases;

/**
 *
 * TestCase1.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 31 Oct 2021
 *
 */
public class TestCase2 implements TestCase {

	//source http://www.gibell.net/pegsolitaire/Catalogs/English33/-11to-11in16_9sweep.gif
	static String[] targetBoards ={
			"111111001111111111111111111111111",//1 move
			"111111010011111111111111111111111",//2 moves
			"111111001011101111110111111111111",//4 moves
			"111111010000101111110111111111111",//6 moves
			"111111011000101011110101111111111",//7 moves
			"111111000100101011110101111111111",//8 moves
			"110110001000101011110101111111111",//10 moves
			"110110001000101011110110011111111",//11 moves
			"001000000100000001111010111111111",//16 moves
			"001000000100000001111011001111111",//17 moves  BSF no
			"001000000110000001000000000000000",//28 moves  BSF no
			"001001000100000000000000000000000",//29 moves  BSF no
			"000000000110000000000000000000000",//30 moves  BSF no        DFS-Recursive 1.2 sec
			"000000001000000000000000000000000",//31 moves  BSF no    DFS-Recursive 1.3 sec
			//other long tests
			"110110001010001010100000000111111",//17 moves  BSF no    DFS-Recursive 7.6 minutes
			"011100000000000000000000000000000",//29 moves  BSF no    DFS-Recursive 1.6 minutes
	};

	static String startBoard = "111111110111111111111111111111111";
	static int[] numberOfMoves = {1,2,4,6,7,8,10,11,16,17,28,29,30,31,17,29};

	@Override
	public String[] targetBoards() {
		return targetBoards;
	}

	@Override
	public String startBaord() {
		return startBoard;
	}

	@Override
	public int[] numberOfMoves() {
		return numberOfMoves;
	}
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test.java.algos;

import static org.junit.Assert.assertTrue;

import java.util.BitSet;
import java.util.List;

import org.junit.jupiter.api.Test;

import main.java.algos.AStar;
import main.java.algos.Algo;
import main.java.algos.BfsIterative;
import main.java.algos.DfsIterative;
import main.java.algos.DfsRecursive;
import test.java.algos.test_cases.TestCase;
import test.java.algos.test_cases.TestCase1;

/**
 *
 * BenchMarkAlgos.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 8 Nov 2021
 *
 */
class BenchMarkAlgos {

	@Test
	void test() {
		runTests(new TestCase1(), 9);
	}

	private void runTests(TestCase test, int testNumber ) {
		String start = test.startBaord();
		String target = test.targetBoards()[testNumber];
		int expectedLength = test.numberOfMoves()[testNumber];
		System.out.println("\n Calculate "+expectedLength+ " moves on Solitair-Peg board:");
		bfsIterative(start, target, expectedLength);
		dfsIterative(start, target, expectedLength);
		dfsRecursive(start, target, expectedLength);
		System.out.println();
		aStar(start, target, expectedLength);
	}

	private void bfsIterative(String start, String target, int expectedLength) {
		Algo algo = new BfsIterative(start, target);
		testCase(algo, expectedLength);
	}

	private void dfsIterative(String start, String target, int expectedLength) {
		Algo algo = new DfsIterative(start, target);
		testCase(algo, expectedLength);
	}

	private void dfsRecursive(String start, String target, int expectedLength) {
		Algo algo = new DfsRecursive(start, target);
		testCase(algo, expectedLength);
	}

	private void aStar(String start, String target, int expectedLength) {
		Algo algo = new AStar(start, target);
		testCase(algo, expectedLength);
	}

	static void testCase(Algo algo,  int numberOfMoves){

		long startTime = System.currentTimeMillis();

		assertTrue(algo.solve());

		List<BitSet> path = algo.getSolution();
		int pathSize = path.size()-1;
		assertTrue(" wrong path length "+pathSize+". Expected "+numberOfMoves,  pathSize  == numberOfMoves);

		System.out.print(" "+algo.getClass().getSimpleName()+" algorithem: ");
		AlgoTestUtils.printDuration((double)System.currentTimeMillis() - startTime);
	}
}

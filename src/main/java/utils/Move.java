package main.java.utils;

public enum Move {

	RIGHT(1,0,2,0), LEFT(-1,0,-2,0),UP(0,-1,0,-2),DOWN(0,1,0,2) ;
	//dx1, dy1 - move to adjacent cell
	//dx2,dy2  - move to final cell
	public int dx1,dy1,dx2,dy2;
	Move(int dx1, int dy1, int dx2, int dy2) {
		this.dx1 = dx1;
		this.dy1 = dy1;
		this.dx2 = dx2;
		this.dy2 = dy2;
	}

	public static Move opposite(Move move){
		switch (move) {
			case LEFT: return RIGHT;
			case RIGHT: return LEFT;
			case UP: return DOWN;
			case DOWN: default: return UP;
		}
	}
}
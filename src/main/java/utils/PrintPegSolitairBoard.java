/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.utils;

import java.util.BitSet;

/**
 *
 * PrintPegSolitairBoard.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 9 Nov 2021
 *
 */
public class PrintPegSolitairBoard {

	/*cahr representation of the three states of a board cell
	  special chars are used to get nice printout
	 */
	public final static char  EMPTY = 9898, PEG = 9899, BORDER = 10062;

	public final static int BOARD_SIZE=PegSolitaireUtils.BOARD_SIZE,
								NUMBER_OF_PEG_HOLES = PegSolitaireUtils.NUMBER_OF_PEG_HOLES;

	/**
	 * @param board representing a {@value #NUMBER_OF_PEG_HOLES} holes board
	 */
	public static char[][] boardCharRepresentation(BitSet board){

		if(board.length() > NUMBER_OF_PEG_HOLES)
			throw new IllegalArgumentException(
					"Invalid string length "+board.length()+ " (should be <="+NUMBER_OF_PEG_HOLES+")");

		char[][] boardOfChars = new char[BOARD_SIZE][BOARD_SIZE];
		int bitSetIndex=0;

		for(int y=0;y<BOARD_SIZE;y++){
			for(int x=0;x<BOARD_SIZE;x++){
				if(PegSolitaireUtils.isWithinBoard(x,y)) {
					boardOfChars[y][x] = board.get(bitSetIndex++) ? PEG : EMPTY;
				}else{
					boardOfChars[y][x] = BORDER;
				}
			}
		}
		return boardOfChars;
	}

	/**
	 * @param board representing a {@value PegSolitaireUtils#NUMBER_OF_PEG_HOLES} holes board
	 */
	public static String toString(BitSet board) {
		StringBuilder sb = new StringBuilder();
		for (char[] row : boardCharRepresentation(board)) {
			sb.append(row);
			sb.append("\n"); //new line
		}
		return sb.toString();
	}
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.utils;

import java.util.BitSet;

/**
 *
 * Board.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 6 Nov 2021
 *
 */
public class Board extends BitSet {

	/**
	 * Board's ancestor
	 */
	private Board parent;
	private int cost;

	public Board() {
		super(PegSolitaireUtils.NUMBER_OF_PEG_HOLES);
		parent = null;
		cost = 0;
	}

	/**
	* Get {@link #parent}
	*/
	public Board getParent() {
		return parent;
	}

	/**
	* Set {@link #parent}
	*/
	public void setParent(Board parent) {
		this.parent = parent;
	}

    /**
	* Get {@link #cost}
	*/
	public int getCost() {
		return cost;
	}

	/**
	* Set {@link #cost}
	*/
	public void setCost(int cost) {
		this.cost = cost;
	}

	@Override
	public Object clone() {
    	Board board = new Board();
    	board.or(this);
    	board.setParent(parent);
    	return board;
    }
}

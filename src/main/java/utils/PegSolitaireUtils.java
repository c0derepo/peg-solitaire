/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.utils;

import java.awt.Point;
import java.util.BitSet;

/**
 *
 * PegSolitaireUtils.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 26 Oct 2021
 *
 */
public class PegSolitaireUtils {

	public final static int BOARD_SIZE=7, CORNER_SIZE=2;
	public final static int NUMBER_OF_PEG_HOLES = BOARD_SIZE*BOARD_SIZE - 4* CORNER_SIZE*CORNER_SIZE;

	public static Integer[][] BOARD_33 = {
		{null,null,0,1,2,null,null},
		{null,null,3,4,5,null,null},
		{ 6, 7, 8, 9,10,11,12},
		{13,14,15,16,17,18,19},
		{20,21,22,23,24,25,26},
		{null,null,27,28,29,null,null},
		{null,null,30,31,32,null,null},
	};

	private static Point[] PEG_HOLES_XY ={
			new Point(2, 0), new Point(3,0), new Point(4,0),
			new Point(2, 1), new Point(3,1), new Point(4,1),
			new Point(0, 2), new Point(1,2), new Point(2, 2), new Point(3,2), new Point(4,2),new Point(5,2), new Point(6,2),
			new Point(0, 3), new Point(1,3), new Point(2, 3), new Point(3,3), new Point(4,3),new Point(5,3), new Point(6,3),
			new Point(0, 4), new Point(1,4), new Point(2, 4), new Point(3,4), new Point(4,4),new Point(5,4), new Point(6,4),
			new Point(2, 5), new Point(3,5), new Point(4,5),
			new Point(2, 6), new Point(3,6), new Point(4,6),
	};

	/**
	 * @throws IllegalArgumentException if s is not valid.
	 * tested using {@link #isValid(String)};
	 */
	public static Board boardFromString(final String s) {
		if(!isValid(s)) throw new IllegalArgumentException("Invalid string "+s);

		Board board = new Board();
		board.set(0, NUMBER_OF_PEG_HOLES);

		char[] chars = s.toCharArray();
		for(int index = 0; index < chars.length; index++){
			if(chars[index] =='0'){
				board.clear(index);
			}
		}
		return board;
	}


	/**
	 * is x,y representing a valid peg location in a
	 * {@value #NUMBER_OF_PEG_HOLES} holes board
	 */
	public static boolean isWithinBoard(int x, int y){

		//check bounds
		if (x < 0 || y < 0 || x >= BOARD_SIZE || y >= BOARD_SIZE) return false;
		//left top corner
		if (x < CORNER_SIZE && y < CORNER_SIZE) return false;
		//right top corner
		if(x >= BOARD_SIZE - CORNER_SIZE && y < CORNER_SIZE) return false;
		//left bottom corner
		if(x < CORNER_SIZE && y >= BOARD_SIZE - CORNER_SIZE) return false;
		//right bottom corner
		if(x >= BOARD_SIZE - CORNER_SIZE && y >= BOARD_SIZE - CORNER_SIZE) return false;

		return true;
	}

	/**
	 * return {@link Point} (Point(x,y)) of representing column - row in a
	 * {@value #NUMBER_OF_PEG_HOLES} peg-holes board
	 */
	public static Point indexToXY(int index){
		if(index < 0 || index >= PEG_HOLES_XY.length) throw new IllegalArgumentException(
			"Invalid index "+index+ " (should be <"+NUMBER_OF_PEG_HOLES+" and >=0 )");

		return PEG_HOLES_XY[index];
	}

	/**
	 * @return
	 * index in a {@link BitSet} representing a {@value #NUMBER_OF_PEG_HOLES}
	 * peg-holes board, for a given x - y (column - row) in that board.
	 */
	public static int xyToIndex(int x, int y){

		if(!isWithinBoard(x, y))
			throw new IllegalArgumentException(x+"-"+y+ " is not a valid peg hole");

		return BOARD_33[y][x];
	}

	/**
	 * Check if a peg, represented by an index in the board can move to direction
	 *
	 */
	public static boolean canMove(BitSet board, int index, Move direction){

		if (! board.get(index)) return false ; //check if there is peg in index
		Point center = indexToXY(index);

		int adjacentIndex = adjacentIndex(center.x, center.y, direction);
		int finalIndex = nextToaAjacentIndex(center.x, center.y, direction);
		if(adjacentIndex < 0 || finalIndex < 0) return false;

		//check if adjacent is a peg and final is empty
		if (board.get(adjacentIndex) && ! board.get(finalIndex)) //adjacent peg and empty hole next
			return true;

		return false;
	}

	/**
	 * @return the index of the adjacent peg-hole in the given direction.
	 * returns -1 if the adjacent peg-hole in not within board
	 */
	public static int adjacentIndex(int x, int y, Move direction){
		//check adjacent location
		int adjacentX = x+direction.dx1;
		int adjacentY = y+direction.dy1;
		if(!isWithinBoard(adjacentX, adjacentY)) return -1;
		return xyToIndex(adjacentX, adjacentY);
	}

	/**
	 * @return the index of the next-to-adjacent (2 holes away) peg-hole in the given direction.
	 * returns -1 if the next-to-adjacent peg-hole in not within board
	 */
	public static int nextToaAjacentIndex(int x, int y, Move direction){
		//check next to adjacent location
		int adjacentX = x+direction.dx2;
		int adjacentY = y+direction.dy2;
		if(!isWithinBoard(adjacentX, adjacentY)) return -1;
		return xyToIndex(adjacentX, adjacentY);
	}

	/**
	 * Applies move without checking validity
	 * @throws IndexOutOfBoundsException if move is not within board
	 */
	public static void move(BitSet board, int index, Move direction){

		Point center = indexToXY(index);
		board.set(index,false);
		board.set(adjacentIndex(center.x, center.y, direction), false);
		board.set(nextToaAjacentIndex(center.x, center.y, direction), true);
	}

	/**
	 * Applies move only if it is a valid one
	 * @return true if move applied
	 */
	public static boolean moveIfValid(BitSet board, int index, Move direction){

		if(!board.get(index)) return false; //empty hole, no peg

		Point center = indexToXY(index);
		int adjacent = adjacentIndex(center.x, center.y, direction);
		int nextToaAjacent = nextToaAjacentIndex(center.x, center.y, direction);

		if(adjacent < 0 || nextToaAjacent < 0   //check if outside board
						|| ! board.get(adjacent) || board.get(nextToaAjacent)  )
																		return false;
		board.set(index,false);
		board.set(adjacent, false);
		board.set(nextToaAjacent, true);
		return true;
	}

	/**
	 * Validates that boardAsString is not null, and if length != {@value #NUMBER_OF_PEG_HOLES}
	 * and consists of 0s and 1s only
	 */
	public static boolean isValid(String boardAsString){
		if (boardAsString == null || boardAsString.length() != NUMBER_OF_PEG_HOLES
												||!boardAsString.matches("[01]+$"))
																		return false;
		return true;
	}

	/**
	 * @return the number of pegs (represented as boolean true) in board
	 */
	public static int numberOfPegs(BitSet board){
		return board.cardinality();
	}

	/**
	 * @return the number of different bits between two {@link BitSet}s
	 */
	public static int numberOfDifferentBits(BitSet a, BitSet b){
		int length = Math.max(a.length(),b.length());
		length = Math.max(length, NUMBER_OF_PEG_HOLES);
		int count = 0;
	    for (int i = 0; i < length; i++) {
	        if (a.get(i) != b.get(i)) {
	            count++;
	        }
	    }

	  return count;
	}
}

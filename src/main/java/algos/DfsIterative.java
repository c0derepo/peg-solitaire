/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.algos;

import java.awt.Point;
import java.util.BitSet;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import main.java.utils.Board;
import main.java.utils.Move;
import main.java.utils.PegSolitaireUtils;

/**
 *
 * BfsIterative.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 30 Oct 2021
 *
 */
public class DfsIterative implements Algo {

	private List<BitSet> solution;
	private final Board startBoard, targetBoard;
	private final int numberOfPegsInTarget;

	/**
	 *
	 */
	public DfsIterative(String startBoardAsString, String targetBoardAsString) {

		startBoard =  PegSolitaireUtils.boardFromString(startBoardAsString);
		targetBoard =  PegSolitaireUtils.boardFromString(targetBoardAsString);
		numberOfPegsInTarget = PegSolitaireUtils.numberOfPegs(targetBoard);
	}

	@Override
	public boolean solve() {

		LinkedList<Board> stack = new LinkedList<>();
		stack.add(startBoard);
		Set<Board>visited = new HashSet<>();

		while(!stack.isEmpty()){

			Board board = stack.peekLast(); //board removed from stack only after all its children were
			//added to the stack
			//check if solve
			if(board.equals(targetBoard)) {
				solution = Algo.ancestorsOf(board);
				return true;
			}

			//if the number of pegs is smaller than in the target board, next move will not
			//lead to a solution because the number of pegs will decrease
			if(PegSolitaireUtils.numberOfPegs(board) <= numberOfPegsInTarget) {
				visited.add(board);
				stack.remove(board);
				continue;
			}

			boolean added = false;
			Board boardCopy = (Board) board.clone();
			boardCopy.setParent(board);


			//search and add all possible next states to queue
			for(int index = 0; index < board.length(); index++){

				Point indexAsXY = PegSolitaireUtils.indexToXY(index);
				//loop to find a valid move. to keep the search "narrow", avoiding wide frontier the
				//loop stops when one valid move is found and added to stack
				for(Move direction : Move.values()){
					if(PegSolitaireUtils.moveIfValid(boardCopy, index, direction)){

						int nextToAdacentIndex = nextToAdacentIndex(indexAsXY, direction);
						int adacentIndex = adacentIndex(indexAsXY, direction);
						if(!visited.contains(boardCopy)) {
							stack.add(boardCopy);
							added = true;
							break;  //add only one next state at each iteration
						}else{
							//undo changes made by moveIfValid
							boardCopy.set(index, true);
							boardCopy.set(adacentIndex, true);
							boardCopy.set(nextToAdacentIndex, false);
						}
					}
				}

				if(added){
					break;
				}
			}

			if(! added){
				visited.add(board);  //mark as visited and remove from queue
				stack.remove(board); //only after fully explored
			}
		}
		return false;
	}

	private int nextToAdacentIndex(Point indexAsXY, Move direction) {
		return PegSolitaireUtils.nextToaAjacentIndex(indexAsXY.x, indexAsXY.y, direction);
	}

	private int adacentIndex(Point indexAsXY, Move direction) {
		return PegSolitaireUtils.adjacentIndex(indexAsXY.x, indexAsXY.y, direction);
	}

	@Override
	public BitSet getStartBoard() {
		return startBoard;
	}

	@Override
	public BitSet getTargetBoard() {
		return targetBoard;
	}

	@Override
	public List<BitSet> getSolution() {
		return solution;
	}
}

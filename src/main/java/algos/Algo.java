/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.algos;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;

import main.java.utils.Board;

/**
 *
 * Algo.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 30 Oct 2021
 *
 */
public interface Algo {

	int MAX_NUMBER_OF_MOVES = 31;

	boolean solve();
	BitSet getStartBoard();
	BitSet getTargetBoard();
	List<BitSet> getSolution();

	default int numberOfMoves(){
		List<BitSet> solution = getSolution();
		return  solution == null ? -1 : solution.size();
	}

	static List<BitSet> ancestorsOf(Board board) {

		List<BitSet> solution = new ArrayList<>();
		solution.add(board);

		while(board.getParent() != null){
			solution.add(board.getParent());
			board = board.getParent();
		}
		Collections.reverse(solution);
		return solution;
	}
}

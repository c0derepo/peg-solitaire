/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.algos;

import java.util.BitSet;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

import main.java.utils.Board;
import main.java.utils.Move;
import main.java.utils.PegSolitaireUtils;

/**
 *
 * AStar.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 30 Oct 2021
 *
 */
public class AStar implements Algo {

	private List<BitSet> solution;
	private final Board startBoard, targetBoard;
	private final int numberOfPegsInTarget;

	/**
	 *
	 */
	public AStar(String startBoardAsString, String targetBoardAsString) {

		startBoard =  PegSolitaireUtils.boardFromString(startBoardAsString);
		targetBoard =  PegSolitaireUtils.boardFromString(targetBoardAsString);
		numberOfPegsInTarget = PegSolitaireUtils.numberOfPegs(targetBoard);
	}

	@Override
	public boolean solve() {

		PriorityQueue<Board> queue =  new PriorityQueue<>(50, (b1,b2)-> Integer.compare(b1.getCost(), b2.getCost()));
		startBoard.setCost(0);
		queue.add(startBoard);
		Set<Board>visited = new HashSet<>();
		visited.add(startBoard);

		while(!queue.isEmpty()){

			Board board = queue.poll(); //get the queue head: the board with lowest cost
			//check if solve
			if(board.equals(targetBoard)) {
				solution = Algo.ancestorsOf(board);
				return true;
			}

			//if the number of pegs is smaller than in the target board, next move will not
			//lead to a solution because the number of pegs will decrease
			if(PegSolitaireUtils.numberOfPegs(board) <= numberOfPegsInTarget) {
				continue;
			}

			//search and add all possible next states to queue
			for(int index = 0; index < board.length(); index++){
				for(Move direction : Move.values()){

					Board boardCopy = (Board) board.clone();
					boardCopy.setParent(board);
					if(PegSolitaireUtils.moveIfValid(boardCopy, index, direction)){
						if(visited.add(boardCopy)) {
							//cost ignores `g` - the distance already traveled
							//and uses only the heuristic to define the cost
							boardCopy.setCost(heuristic(boardCopy));
							queue.add(boardCopy);
						}
					}
				}
			}
		}
		return false;
	}

	@Override
	public BitSet getStartBoard() {
		return startBoard;
	}

	@Override
	public BitSet getTargetBoard() {
		return targetBoard;
	}

	@Override
	public List<BitSet> getSolution() {
		return solution;
	}

	//returns the number of different pegs and holes between board and the target board
	private int heuristic(Board board){
		return PegSolitaireUtils.numberOfDifferentBits(board, targetBoard);
	}
}

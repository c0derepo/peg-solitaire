/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.algos;

import java.awt.Point;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import main.java.utils.Board;
import main.java.utils.Move;
import main.java.utils.PegSolitaireUtils;

/**
 *
 * DfsRecursiveTest.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 30 Oct 2021
 *
 */
public class DfsRecursive implements Algo {

	private final List<BitSet> solution;
	private final Board startBoard, targetBoard;
	private final int numberOfPegsInTarget;

	private final Set<Board>visited = new HashSet<>();

	public DfsRecursive(String startBoardAsString, String targetBoardAsString) {

		startBoard =  PegSolitaireUtils.boardFromString(startBoardAsString);
		targetBoard =  PegSolitaireUtils.boardFromString(targetBoardAsString);
		numberOfPegsInTarget = PegSolitaireUtils.numberOfPegs(targetBoard);
		solution = new ArrayList<>();
	}

	@Override
	public boolean solve() {
		solution.clear();
		return dfs(startBoard);
	}

	private boolean dfs( Board board ){

		if(!visited.add(board))	return false;

		if(board.equals(targetBoard))	{
			solution.add(board);
			return true;
		}

		//if the number of pegs is smaller than in the target board, next move will not
		//lead to a solution because the number of pegs will decrease
		if(PegSolitaireUtils.numberOfPegs(board) <= numberOfPegsInTarget) 	return false;

		solution.add((Board)board.clone());

		for(int index = 0; index < board.length(); index++){

			Point indexAsXY = PegSolitaireUtils.indexToXY(index);
			for(Move direction : Move.values()){
				int nextToAdacentIndex = nextToAdacentIndex(indexAsXY, direction);
				int adacentIndex = adacentIndex(indexAsXY, direction);

				if(PegSolitaireUtils.moveIfValid(board, index, direction)){
					if(dfs(board)) return true;
					else {
						board.set(index, true);
						board.set(adacentIndex, true);
						board.set(nextToAdacentIndex, false);
					}
				}
			}
		}

		solution.remove(board);
		return false;
	}

	private int nextToAdacentIndex(Point indexAsXY, Move direction) {
		return PegSolitaireUtils.nextToaAjacentIndex(indexAsXY.x, indexAsXY.y, direction);
	}

	private int adacentIndex(Point indexAsXY, Move direction) {
		return PegSolitaireUtils.adjacentIndex(indexAsXY.x, indexAsXY.y, direction);
	}

	@Override
	public BitSet getStartBoard() {
		return startBoard;
	}

	@Override
	public BitSet getTargetBoard() {
		return targetBoard;
	}

	@Override
	public List<BitSet> getSolution() {
		return solution;
	}
}
